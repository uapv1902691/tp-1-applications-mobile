package com.example.myapplication;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import data.Country;

public class DetailFragment extends Fragment {

    public static final String TAG = "SecondFragment";
    Context context;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        context = container.getContext();
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());

        int paysId = args.getCountryId();
        TextView pays = view.findViewById(R.id.pays);
        String drapeauValue = Country.countries[paysId].getImgUri();
        ImageView drapeau = view.findViewById(R.id.drapeau);
        drapeau.setImageDrawable(context.getResources().getDrawable(context.getResources().getIdentifier(drapeauValue, null, context.getPackageName())));
        TextView capital = view.findViewById(R.id.capitalvalue);
        TextView monnaie =  view.findViewById(R.id.monnaievalue);
        TextView population =  view.findViewById(R.id.populationvalue);
        TextView superficie =  view.findViewById(R.id.superficievalue);
        TextView langue =  view.findViewById(R.id.languevalue);

        pays.setText(Country.countries[paysId].getName());
        capital.setText(Country.countries[paysId].getCapital());
        monnaie.setText(Country.countries[paysId].getCurrency());
        population.setText(String.valueOf(Country.countries[paysId].getPopulation()));
        superficie.setText(String.valueOf(Country.countries[paysId].getArea()));
        langue.setText(Country.countries[paysId].getLanguage());

        view.findViewById(R.id.button_second).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(DetailFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);
            }
        });
    }
}